import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TitleComponent } from './components/title/title.component';
import { FooterComponent } from './components/footer/footer.component';
import { FormComponent } from './components/form/form.component';
import { LineComponent } from './components/form/line/line.component';
import { AccessoriesComponent } from './components/form/accessories/accessories.component';
import { GeneralComponent } from './components/form/general/general.component';
import { ContractComponent } from './components/form/contract/contract.component';

@NgModule({
  declarations: [
    AppComponent,
    TitleComponent,
    FooterComponent,
    FormComponent,
    LineComponent,
    AccessoriesComponent,
    GeneralComponent,
    ContractComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
