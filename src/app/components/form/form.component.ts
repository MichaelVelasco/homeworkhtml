import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {

  successMsg = false;

  constructor() { }

  ngOnInit(): void {

  }

  submitForm(){
    this.successMsg = true;
    setTimeout(() => {
      this.successMsg = false;
      setTimeout(() => {
        window.location.reload();
      }, 1000);
    }, 4000);
    return false;
  }

}
