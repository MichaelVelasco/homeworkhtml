import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-general',
  templateUrl: './general.component.html',
  styleUrls: ['./general.component.css']
})
export class GeneralComponent implements OnInit {

  brands = [
    'BMW',
    'Mercedes-Benz',
    'Audi',
    'McLaren',
    'Lexus',
    'Renault',
    'Ford',
    'Lamborghini',
  ];

  carImage = '';

  constructor() { }

  ngOnInit(): void {
    this.changeImage();
  }

  changeImage(){
    this.carImage = 'assets/images/car'+Math.floor(Math.random() * 6)+'.jpg';
    setInterval(() => {
      this.carImage = 'assets/images/car'+Math.floor(Math.random() * 6)+'.jpg';
    }, 3000);
  }

  thisYear(){
    return new Date().getFullYear();
  }

}
