import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {

  text = 'Para más información haga click aquí';
  link = 'http://www.konradlorenz.edu.co/es/';

  constructor() { }

  ngOnInit(): void {
  }

}
